const email = document.querySelector("#email");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirmPassword");
const name = document.getElementById("name");
const surname = document.getElementById("surname");
const checkbox = document.getElementById("checkbox-1");
const completed = document.getElementById("completed");
const form = document.querySelector(".main");

form.addEventListener("submit", (event) => {
  event.preventDefault();

  let validAllConditon = true;

  function nameValid() {
    const nameRegex = /^[a-zA-Z ]{2,30}$/;
    if (nameRegex.test(name.value) != true) {
      name.style.border = "1px solid red";
      validAllConditon = false;
    } else {
      name.style.border = "1px solid white";
    }
  }
  nameValid();

  function surnameValid() {
    const surnameRegex = /^[a-zA-Z ]{2,30}$/;
    if (surnameRegex.test(surname.value) != true) {
      surname.style.border = "1px solid red";
      validAllConditon = false;
    } else {
      surname.style.border = "1px solid white";
    }
  }
  surnameValid();

  function emailValid() {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (emailRegex.test(email.value) != true) {
      email.style.border = "1px solid red";
      validAllConditon = false;
    } else {
      email.style.border = "1px solid white";
    }
  }
  emailValid();

  function passwordValid() {
    const passwordRegex = /^[a-zA-Z0-9!@#$%^&*()+-]{8}$/;
    if (passwordRegex.test(password.value) != true) {
      password.style.border = "1px solid red";
      validAllConditon = false;
    } else {
      password.style.border = "1px solid white ";
    }
  }
  passwordValid();

  function passwordMatchValid() {
    if (password.value !== confirmPassword.value) {
      confirmPassword.style.border = "1px solid red";
      validAllConditon = false;
      return false;
    }
    confirmPassword.style.border = "1px solid white";
    return true;
  }
  passwordMatchValid();

  function checkboxValid() {
    if (checkbox.checked == true) {
      checkbox.style.border = "1px solid white";
    } else {
      checkbox.style.border = "1px solid red";
      validAllConditon = false;
    }
  }
  checkboxValid();

  if (validAllConditon == true) {
    completed.textContent = "SignUp successful";
    form.style.display = "none";
  }
  
});
